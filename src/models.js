/**
 * @class CellularAutomaton
 * 
 * @author Matthias Tangemann
 */
var CellularAutomaton = Backbone.Model.extend({
	
	defaults: function() {
		return {
			grid: new Grid(),
			neighbourhood: function(index) { return []; },
			rule: function(state, neighbours) { return state; }
		};
	},
	
	
	update: function() {
		
	}	
});



/**
 * @class Grid
 *
 * @author Matthias Tangemann
 */
var Grid = Backbone.Model.extend({

	defaults: function() {
		return {
			dimension: 0,
			stateClass: State
		};
	},


	//Returns true, if the given index is valid
	isValidIndex: function(index) {
		return false;
	},
	
	
	//Returns the state of the cell at the given index
	getCell: function(index) {
		return null;
	},
	
	
	//Sets the state of the cell at the given index
	setCell: function(index, state) {
		return null;
	},
	
	
	getIterator: function() {
		
	},
	
	clone: function() {
		return new Grid();
	}
});



/**
 * @class GridIterator
 * 
 * @author Matthias Tangemann
 */
var GridIterator = Backbone.Model.extend({
	
	defaults: function() {
		return {
			grid: undefined
		};
	},	
	
	next: function() {
		return undefined;
	},
	
	current: function() {
		return undefined;
	}
});



/**
 * @class RectGrid
 * A grid that contains a constant number of cells in each dimension
 *
 * @author Matthias Tangemann
 */
var RectGrid = Grid.extend({

	constructor: function(limits, stateClass) {
		this.limits = limits;
		
		//Calc the number of cells
		this.linear_limit = 1;
		for (var i = 0; i < limits.length; i++) {
			this.linear_limit *= limits[i];
		}
		this.linear_limit -= 1;
		
		//Create and initialize the cells
		this.cells = new Array(this.linear_limit + 1);
		for (var i = 0; i <= this.linear_limit; i++) {
			this.cells[i] = new stateClass();
		}

		Backbone.Model.apply(this, [{
			dimension: limits.length,
			stateClass: stateClass
		}]);
	},


	isValidIndex: function(index) {
		//Check the dimension of the index
		if (index.length != this.get("dimension")) {
			return false;
		}

		//Check whether the index exceeds a limit in any dimension
		for (var i = 0; i < this.get("dimension"); i++) {
			if (index[i] < 0 || index[i] >= this.limits[i]) {
				return false;
			}
		}

		return true;
	},
	
	
	getCell: function(index) {
		var linear_index = this.mapIndex(index);
		
		if (linear_index >= 0) {
			return this.cells[linear_index];
		}
		
		return undefined;
	},
	
	
	getCellLinear: function(linear_index) {
		if (linear_index < 0 || linear_index > this.linear_limit) {
			return undefined;
		}
		
		return this.cells[linear_index];
	},
	
	
	setCell: function(index, state) {
		var linear_index = this.mapIndex(index);
		
		if (linear_index >= 0) {
			this.cells[linear_index] = state;
		}
	},
	
	
	setCellLinear: function(linear_index, state) {
		if (linear_index < 0 || linear_index > this.linear_limit) {
			return;
		}
		
		this.cells[linear_index] = state;
	},
	
	
	//Maps a n-dimensional index to the 1-dimensional index needed for the cells array
	mapIndex: function(index) {
		if (!this.isValidIndex(index)) {
			return -1;
		}
		
		var factor = 1;
		var linear_index = 0;
		for (var i = this.get("dimension") - 1; i >= 0; i--) {
			linear_index += index[i] * factor;
			factor *= this.limits[i];
		}
		
		return linear_index;
	},
	
	
	getIterator: function() {
		return new RectGridIterator({grid: this});
	},
	
	
	clone: function() {
		var grid = new RectGrid(this.limits, this.get("stateClass"));
			
		for (var i = 0; i <= this.linear_limit; i++) {
			grid.setCellLinear(i, this.cells[i].clone());
		}
			
		return grid;	
	}
});



var RectGridIterator = GridIterator.extend({
	initialize: function() {
		this.index = 0;
	},
	
	current: function() {
		return this.get("grid").getCellLinear(this.index);
	},
	
	next: function() {
		if (this.current() == undefined) {
			return undefined;
		}
		
		console.log(this.get("grid").getCellLinear(1));
		this.index += 1;
		return this.get("grid").getCellLinear(this.index);
	}
});



/**
 * @class State
 * 
 * @author Matthias Tangemann
 */
var State = Backbone.Model.extend({
	
	//Returns a string that represents the state
	toString: function() {
		return "";
	},
	
	clone: function() {
		return new State();
	}
	
});
